local discordia = require('discordia')
local client = discordia.Client()
local prefix = "pr:"
discordia.extensions()
-- Setting up the client
client:on('ready', function()
  print('Logged in as '.. client.user.username)
  client:setGame("pr:help")
end)
-- Help Command
client:on('messageCreate', function (message)
  local author = message.author
  if message.content == prefix .. "help" then
    message:reply {
      embed ={title = "Help",description = "Here is the list of commands in George Washinbot!",author = {name = author.username,icon_url = author.avatarURL},
        fields = {
          {name = "Whois",value = "The Bot will find information about the person you mentioned!",inline = false},
          {name = "Kick",value = "This will kick a specified member. Be sure to have the permissions though!",inline = false},
          {name = "Ban",value = "This will ban a specified member. Be sure to have the permissions though!",inline = false},
          {name = "Magicball",value = "Help you answer your questions by giving a random answer!",inline = false},
          {name = "Pfp",value = "This will give you the profile picture of a specified user!",inline = true},
          {name = "Purge",value = "Purges a specified amount of messages. Be sure to have the permissions though!",inline = false}},
        footer = {text = "Created with Lua and Discordia!\nThe Prefix for this server is " .. prefix},
        color = 0xcc241d}}
    end
end)
-- Ban Command
client:on('messageCreate', function(message)
  if message.author.bot then return end
  if message.content:sub(1, 6) == prefix .. 'ban' then
    local author = message.guild:getMember(message.author.id)
    local member = message.mentionedUsers.first
    if not member then
      message:reply("Please mention someone to ban!")
      return
    elseif not author:hasPermission("banMembers") then
      message:reply("You do not have the 'banMembers' permissions! Shoo Demon!!")
    elseif message.author == message.mentionedUsers.first then
      message:reply("You cannot ban yourself!")
      return
    end
    for user in message.mentionedUsers:iter() do
        member = message.guild:getMember(user.id)
          if member:ban() then
          message.channel:send(member.username .. " has been banned! ID: " .. member.id)
        end
      end
    end
end)
-- Kick Command
client:on('messageCreate', function(message)
  if message.author.bot then return end
  if message.content:sub(1, 7) == prefix .. 'kick' then
    local author = message.guild:getMember(message.author.id)
    local member = message.mentionedUsers.first
    if not member then
      message:reply("Please mention someone to kick!")
      return
    elseif not author:hasPermission("kickMembers") then
      message:reply("You do not have the 'kickMembers' permissions! Shoo Demon!!")
      return
    elseif message.author == message.mentionedUsers.first then
      message:reply("You cannot kick yourself!")
      return
    end
    for user in message.mentionedUsers:iter() do
        member = message.guild:getMember(user.id)
          if member:kick() then
          message.channel:send(member.username .. " has been kicked! ID:" .. member.id)
      end
    end
  end
end)
-- Magicball Command
client:on('messageCreate', function(message)
  local author = message.author
  local answers = {
          'Yes',
          'No',
          'Maybe',
          'Maybe not',
          'Probably ',
          'Probably not.'
        }
  if message.content:sub(1, 12) == prefix .."magicball" then
    message:reply {
      embed = {title = ":8ball: Magic Ball",description = "Question:\n".. message.content:gsub(prefix .. "magicball ", ""),
        fields = {{name = "Answer:", value = answers[math.random(1, #answers)]}},
      footer = {text = "Asked by " .. author.username,icon_url = author.avatarURL},
      color = 0xb16286
    }
  }
  end
end)
-- Profile picture command
client:on("messageCreate", function(message)
  if message.content:sub(1, 6) == prefix .. "pfp" then
    local member = message.mentionedUsers.first
    local author = message.author
    if not member then
      message:reply {
        embed = {author = {name = author.username,icon_url = author.avatarURL},title = "Profile Picture",image = {url = author.avatarURL},
        color=0x282828}}
      else
        message:reply {
          embed = {author = {name = member.username,icon_url = member.avatarURL},title = "Profile Picture",image = {url = member.avatarURL}},
          color = 0x282828}
    end
  end
end)
-- Whois command
client:on("messageCreate", function(message)
  if message.content:sub(1, 8) == prefix .. "whois" then
    local member = message.mentionedUsers.first
    if not member then
      message:reply("Please mention a person to view their information!")
      return
    end
    for user in message.mentionedUsers:iter() do
      member = message.guild:getMember(user.id)
        message:reply {
          embed = {thumbnail = {url = member.avatarURL},
	    fields = {
		{name = 'Nick & Tag', value = member.tag, inline = true},
                {name = 'ID', value = member.id, inline = false},
                {name = 'Highest Role', value = member.highestRole.name, inline = true},
		{name = 'Boosted Since', value = member.premiumSince or "Not Boosted", inline = false},
		{name = 'Joined Server At', value = member.joinedAt and member.joinedAt:gsub('%..*', ''):gsub('T', ' ') or '?', inline = false},
		{name = 'Joined Discord At', value = discordia.Date.fromSnowflake(member.id):toISO(' ', ''), inline = true},
              },
            color = 0x458588}}
    end
  end
end)
-- Mission Command
client:on("messageCreate", function(message)
  if message.content == prefix .. "mission" then
    local author = message.guild:getMember(message.author.id)
        if not author:hasPermission("kickMembers") then
        return
      end
    message:reply {
      embed = {
      title = "Imperial Reformation",
      image = {
        url = "https://imgur.com/VT0GVWu.png",
      },
      fields = {
        {name = 'Description', value = "It is time for us to reform the Empire.", inline = false},
        {name = 'Requirements', value = "At Least 75 members is needed for this mission to be completed", inline = true}
      },
      color = 0xffc80f}}
  end
end)
-- Purge Command
client:on("messageCreate", function(message)
  if message.content:sub(1, 8) == prefix .. "purge" then
          if message.member:hasPermission('manageMessages') then
          local repla = string.gsub(message.content, prefix.."purge ", "")
          local cha = message.guild:getChannel(message.channel.id)
          if not cha:bulkDelete(message.channel:getMessagesBefore(message.id, repla)) then
            message:delete()
            local reply = message:reply("Something went wrong!")
            discordia.Clock():waitFor("", 3000)
            reply:delete()
          else
            message:delete()
            local reply
            if repla == "1" then
              reply = message:reply("Chat purged by <@!"..message.author.id.."> (Purged "..repla.." Message)!")
            else
              reply = message:reply("Chat purged by <@!"..message.author.id.."> (Purged "..repla.." Messages)!")
            end
            discordia.Clock():waitFor("", 3000)
            reply:delete()
          end
        else
          message:reply("Sorry <@!"..message.author.id.."> you dont have perms to purge!")
        end
      end
    end)
-- Replacement for Ping Command
client:on("messageCreate", function(message)
        local author = message.author
        if author.bot then return end
        if message.content == "hello" then
          message:reply( "Hello " .. author.username .. "!")
        else if message.content == "Hi" then
          message:reply( "Hello " .. author.username .. "!")
        else if message.content == "Hello" then
          message:reply( "Hello " .. author.username .. "!")
        else if message.content:sub(1,6) == "hello " then
          message:reply( "Hello " .. author.username .. "!")
        else if message.content:sub(1,3) == "Hi " then
          message:reply( "Hello " .. author.username .. "!")
        else if message.content:sub(1,6) == "Hello " then
          message:reply( "Hello " .. author.username .. "!")
            end
          end
         end
      end
   end
  end
end)
-- Running the Client
client:run(args[2])
